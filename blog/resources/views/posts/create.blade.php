@extends('layout.master')

@section('content')

        <div class="ml-4 md-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/posts" method="POST">
                @crsf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judul" placeholder="Masukan judul pertanyaan">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" placeholder="Isi pertanyaan">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
            

@endsection