@extends('layout.master')

@section('content')
    <div class="ml-4 md-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$question->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$question->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul Pertanyaan</label>
                        <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul',$question->judul)}}" placeholder="Masukan judul pertanyaan">
                         @error('judul')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror  
                    </div>
                    <div class="form-group">
                        <label for="isi">Isi Pertanyaan</label>
                        <input type="text" class="form-control" id="isi" name="isi" value="{{old('judul',$question->isi)}}" placeholder="Isi pertanyaan">
                        @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror  
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
        </div>
@endsection