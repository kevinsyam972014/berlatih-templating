@extends('layout.master')

@section('content')

    <div class="ml-4 mt-3">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Table List Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <a class="btn btn-primary mb-3" href="/pertanyaan/create">Buat Pertanyaan</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi Pertanyaan</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      @forelse ($pertanyaan as $key => $question)
                          <tr>
                              <td>{{$key + 1}}</td>
                              <td>{{$question->judul}}</td>
                              <td>{{$question->isi}}</td>
                              <td style="display: flex">
                                  <a class="btn btn-info btn-sm" href="/pertanyaan/{{$question->id}}">show</a>
                                  <a class="btn btn-default btn-sm" href="/pertanyaan/{{$question->id}}/edit">edit</a>
                                  <form action="/pertanyaan/{{$question->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                    </form>
                              </td>
                          </tr>
                        @empty
                        <tr>
                            <td colspan="4" align="center">Tidak ada Pertanyaan</td>
                        </tr>

                      @endforelse
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!--<div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>-->
            </div>
    </div>

@endsection