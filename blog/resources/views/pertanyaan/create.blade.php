@extends('layout.master')

@section('content')

        <div class="ml-4 md-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul Pertanyaan</label>
                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukan judul pertanyaan">
                         @error('judul')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror  
                    </div>
                    <div class="form-group">
                        <label for="isi">Isi Pertanyaan</label>
                        <input type="text" class="form-control" id="isi" name="isi" placeholder="Isi pertanyaan">
                        @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror  
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
            

@endsection