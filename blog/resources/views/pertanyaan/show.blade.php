@extends('layout.master')

@section('content')
    <div class="mt-3 ml-4">
        <h4>{{$question->judul}}</h4>
        <p>{{$question->isi}}</p>
    </div>    

@endsection